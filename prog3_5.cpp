extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}


#include <iostream>
#include <stdio.h>
#include <string>
#include <stdlib.h>

using namespace std;

void print_error(lua_State *state) {
    const char *message = lua_tostring(state, -1);
    puts(message);
    lua_pop(state, 1);
}

void execute(const char *filename) {
    lua_State *state = luaL_newstate();
    luaL_openlibs(state);
    int result;
    result = luaL_loadfile(state, filename);
    if (result != LUA_OK){
        print_error(state);
        return;
    }
    result = lua_pcall(state, 0, LUA_MULTRET, 0);
    if (result != LUA_OK) {
        print_error(state);
        return;
    }
}

int main(int argc, char *argv[]) {
    cout << "Assignment #3-3, Daniel Kelchner, danjoe.kelch@gmail.com" << endl;
    if (argc <= 1) {
        puts("Usage: runlua file(s)");
        puts("Loads and executes Lua programs.");
        return 1;
    }
    for (int n = 1; n < argc; n++) {
        execute(argv[n]);
    }
    int status, result;
    char strUsr[256];
    scanf("%[^\n]s", strUsr);
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    status = luaL_loadfile(L, "prog3_2.lua");
    if (status) {
        fprintf(stderr, "Couldn't load the file: %s\n", lua_tostring(L, -1));
        exit(1);    
    }
    lua_newtable(L);
    lua_pushnumber(L, 1);
    lua_pushstring(L, strUsr);
    lua_rawset(L, -3);
    lua_setglobal(L, "InfixToPostfix");
    result = lua_pcall(L, 0, LUA_MULTRET, 0);
    if (result) {
        fprintf(stderr, "Failed to run script: %s\n", lua_tostring(L, -1));
        exit(1);
    }
    const char *strResult = lua_tostring(L, -1);
    printf("%s\n", strResult);
    lua_pop(L, 1);
    lua_close(L);
    return 0;
}
