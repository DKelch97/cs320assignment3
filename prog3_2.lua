function splitStringOnDelim(inStr, sep)
    if (sep == nil) then
        sep = "%s"
    end
    local t = {} ; i = 1
    for str in string.gmatch(inStr, "([^"..sep.."]+)") do
        t[i] = str
        i = i +1
    end
    return t
end

--The, "InfixToPostfix," function, below, converts an infix string into a postfix string.

function InfixToPostfix(inputqueue)
    prec = {}
    opStack = {}
    outputqueue = {}
    --Both table, "opStack," and, "outputqueue," are given indexes of name, "mySize." Being as
    --I was unable to find a pre-established function which would present me with the size of 
    --a table, this seemed to be the most simplistic approach to solving the problem.
    opStack["mySize"] = 0
    outputqueue["mySize"] = 0
    --Table, "prec," functions primarily as a dictionary, wherein strings, "+," "-," "*," and "/" 
    --serve as indexes. This will later work to help us perform comparisons between two operators.
    prec["+"] = 1
    prec["-"] = 1
    prec["*"] = 2
    prec["/"] = 2
    s = splitStringOnDelim(inputqueue, "%s")
    for k, v in ipairs(s) do
        if ((v == "+") or (v == "-") or  (v == "*") or  (v == "/")) then
            while (opStack["mySize"] > 0 and prec[opStack[opStack["mySize"]]] >= prec[v]) do
                outputqueue["mySize"] = outputqueue["mySize"] + 1
                table.insert(outputqueue, outputqueue["mySize"], opStack[opStack["mySize"]])
                table.remove(opStack)
                opStack["mySize"] = opStack["mySize"] - 1
            end  
            opStack["mySize"] = opStack["mySize"] + 1
            table.insert(opStack, opStack["mySize"], v)   
        else
            outputqueue["mySize"]  = outputqueue["mySize"] + 1
            table.insert(outputqueue, outputqueue["mySize"], v)
        end
    end
    --We account for the fact that there may remain operators with the, "opStack," even after
    --processing all elements from the table, "s."
    while(opStack["mySize"] > 0) do
        outputqueue["mySize"] = outputqueue["mySize"]  + 1
        table.insert(outputqueue, outputqueue["mySize"], opStack[opStack["mySize"]])
        table.remove(opStack) 
        opStack["mySize"] = opStack["mySize"] - 1
    end 
    print("Assignment #3-2, Daniel Kelchner, danjoe.kelch@gmail.com")
    outString = ""
    for i,v in ipairs(outputqueue) do
        outString = outString .. " " .. v    
    end
    return outString
end

