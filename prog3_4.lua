function splitStringOnDelim(inStr, sep)
    if (sep == nil) then
        sep = "%s"
    end
    local t = {} ; i = 1
    for str in string.gmatch(inStr, "([^"..sep.."]+)") do
        t[i] = str
        i = i +1
    end
    return t
end
    inputqueue = "3 + 5 * 6 / 2 + 1"
    prec = {}
    opStack = {}
    outputqueue = {}
    opStack["mySize"] = 0
    outputqueue["mySize"] = 0
    prec["+"] = 1
    prec["-"] = 1
    prec["*"] = 2
    prec["/"] = 2
    s = splitStringOnDelim(inputqueue, "%s")
    for k, v in ipairs(s) do
        if ((v == "+") or (v == "-") or  (v == "*") or  (v == "/")) then
            while (opStack["mySize"] > 0 and prec[opStack[opStack["mySize"]]] >= prec[v]) do
                outputqueue["mySize"] = outputqueue["mySize"] + 1
                table.insert(outputqueue, outputqueue["mySize"], opStack[opStack["mySize"]])
                table.remove(opStack)
                opStack["mySize"] = opStack["mySize"] - 1
            end  
            opStack["mySize"] = opStack["mySize"] + 1
            table.insert(opStack, opStack["mySize"], v)   
        else
            outputqueue["mySize"]  = outputqueue["mySize"] + 1
            table.insert(outputqueue, outputqueue["mySize"], v)
        end
    end
    while(opStack["mySize"] > 0) do
        outputqueue["mySize"] = outputqueue["mySize"]  + 1
        table.insert(outputqueue, outputqueue["mySize"], opStack[opStack["mySize"]])
        table.remove(opStack) 
        opStack["mySize"] = opStack["mySize"] - 1
    end 
    print("Assignment #3-2, Daniel Kelchner, danjoe.kelch@gmail.com")
    outString = ""
    for i,v in ipairs(outputqueue) do
        outString = outString .. " " .. v    
    end
    return outString

