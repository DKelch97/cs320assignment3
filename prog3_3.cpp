extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}


#include <iostream>
#include <stdio.h>
#include <string>
#include <stdlib.h>

using namespace std;

void print_error(lua_State *state) {
    const char *message = lua_tostring(state, -1);
    puts(message);
    lua_pop(state, 1);
}

//The below subroutine pushes a user entered (infix) string to the lua function,
//"InfixToPostfix(str)," of file, "prog3_2.lua." The returned (postfix) string of said
//function is then printed.

void execute(const char *filename) {
    lua_State *state = luaL_newstate();
    luaL_openlibs(state);
    char strUsr[256];
    scanf("%[^\n]s", strUsr);
    int result;
    luaL_dofile(state, filename);
    const char *strResult;
    lua_getglobal(state, "InfixToPostfix");
    lua_pushstring(state, strUsr);
    result = lua_pcall(state, 1, 1, 0);
    if (result != LUA_OK) {
        print_error(state);
        return;
    }
    strResult = lua_tostring(state, -1);
    printf("%s\n", strResult);
    lua_pop(state, 1);
    lua_close(state);
}

int main(int argc, char *argv[]) {
    cout << "Assignment #3-3, Daniel Kelchner, danjoe.kelch@gmail.com" << endl;
    if (argc <= 1) {
        puts("Usage: runlua file(s)");
        puts("Loads and executes Lua programs.");
        return 1;
    }
    //Much like prog3_1.cpp, we utilize the function, "execute(const char *filename)," to open a 
    //lua environment, then run the designated lua file (filename).
    execute(argv[1]);
    return 0;
}
