extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include <iostream>
#include <stdio.h>

using namespace std;

void print_error(lua_State *state) {
    const char *message = lua_tostring(state, -1);
    puts(message);
    lua_pop(state, 1);
}

//The, "execute," function, below, opens a lua environment within which the
//noted lua file, "filename," will be run.

void execute(const char *filename) {
    lua_State *state = luaL_newstate();
    luaL_openlibs(state);
    int result;
    result = luaL_loadfile(state, filename);
    if (result != LUA_OK){
        print_error(state);
        return;
    }
    result = lua_pcall(state, 0, LUA_MULTRET, 0);
    if (result != LUA_OK) {
        print_error(state);
        return;
    }
}
int main(int argc, char *argv[]) {
    cout << "Assignment #3-1, Daniel Kelchner, danjoe.kelch@gmail.com" << endl;
    if (argc <= 1) {
        puts("Usage: runlua file(s)");
        puts("Loads and executes Lua programs.");
        return 1;
    }
    //For each of the designated command line arguments, starting with argv[1],
    //we call the function, "execute."
    for (int n = 1; n < argc; n++) {
        execute(argv[n]);
    }
    return 0;
}
