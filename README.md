Daniel Joseph Kelchner

danjoe.kelch@gmail.com

Description of Included Files:

a. prog3_1.cpp
    
    This c++ program takes in a single command line argument, the name of a lua file,
    and executes it within a lua environment. The basis for this program can be found 
    on https://csl.name/post/lua-and-cpp/

b. prog3_2.lua
    
    Within this file, we implement a function which converts in infix string, into a
    postfix string. This functon InfixToPostfix(str), depends heavily upon the usage
    of tables indexed by strings ("*", "/", "+", "-"), otherwise known as dictionaries.

c. prog3_3.cpp

    Much like prog3_1.cpp, we run the file denoted by the command line argument of argv[1]
    within a lua environment. We pass a designated string to the InfixToPostfix(str)
    function of, "prog3_2.lua," and print the returned string of said function when 
    we re-enter the c++ environment.
